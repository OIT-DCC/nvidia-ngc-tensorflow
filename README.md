# nvidia-ngc-tensorflow

This project builds a Singularity image based on the TensorFlow image provided by NVidia NGC

* * The Singularity images created are stored on research-singularity-registry.oit.duke.edu. You can pull your image down by _curl_ (_e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/nvidia-ngc-tensorflow.sif_).

* To use the image you would include ""singularity exec"" or "singularity run", whichever your Singularity.def file has set up in your submit scripts in the Duke Compute Cluster. For example:
``` YAML
# !/Bin/Bash
# SBATCH -E slurm.err
singularity exec --nv nvidia-ngc-tensorflow.sif some_script.py
```
